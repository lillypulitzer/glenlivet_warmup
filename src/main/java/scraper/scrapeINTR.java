package scraper;

import org.htmlparser.util.NodeList;

import java.util.Collection;


/**
 * Created by whill on 12/18/15.
 * ...
 */
public interface scrapeINTR {

    /**
     * Returns the number of visited links
     * @return size
     */
    int size();

    /**
     * Checks if a link was already visited
     * @param link
     * @return if link has been visited
     */
    boolean visited(String link);

    /**
     * Marks a link as visited
     * @param link
     */
    void addVisited(String link);

    /**
     * Adds a link to be processed by Glenlivet
     * @param link
     */
    void addToGlenlivetCallList(String link);

    Collection<String> aCallListToGlenlivet();
}
