package scraper;

/**
 * Created by whill on 12/18/15.
 * ..
 */

import java.io.UnsupportedEncodingException;
import java.net.*;
import java.util.*;
import java.util.concurrent.ForkJoinPool;

public class scrapeIMPL implements scrapeINTR{

    private final Collection<String>
            visitedLinks = Collections.synchronizedSet(new HashSet<String>()),
            makeCallsToGlenlivet = Collections.synchronizedSet(new HashSet<String>());
    String
        uriLink,
        devGlenlivetProductsPrefix = "http://dev1.lilly.api.prolific.io/products/",
        prodGlenlivetProductsPrefix = "https://mobile-api.lillypulitzer.com/products/";
    ForkJoinPool mainPool;

    public scrapeIMPL(String startingURL, int maxThreads) {
        this.uriLink = startingURL;
        mainPool = new ForkJoinPool(maxThreads);
    }

    private void callGlenlivet(ArrayList<String> callList) throws Exception {
        Iterator clI = callList.iterator();
        while (clI.hasNext()) {
            URL callTo = new URL(prodGlenlivetProductsPrefix + (String)clI.next());
            //Parser parser = new Parser(callTo.openConnection());
            //System.out.println("Moving on...");
            callTo.openStream();
        }
    }

    public void buildGlenlivetCallURIs() throws UnsupportedEncodingException, URISyntaxException {
        Collection<String> links = aCallListToGlenlivet();
        Iterator testIterator = links.iterator();
        ArrayList<String> encodedPath = new ArrayList<>();
        while (testIterator.hasNext()) {
            URI uri = new URI((String)testIterator.next());
            String tempEncodedPath = URLEncoder.encode(
                    URLEncoder.encode(uri.getPath().replaceFirst("/", ""), "ASCII"), "ASCII").toLowerCase();
            encodedPath.add(tempEncodedPath);
        }
        try {
            callGlenlivet(encodedPath);
        } catch (Exception e ) {
            System.err.println(e);
        }
    }

    public void startCrawling() {
        System.out.println("startCrawling");
        //non-recursive scrape
        linkFinderActionIMPL lfa = new linkFinderActionIMPL(this.uriLink, this);
        lfa.scrapeTopLevel();
        try{
            buildGlenlivetCallURIs();
        } catch(Exception e) {
            System.err.println(e);
        }
        //nested/recursive scrape
        //mainPool.invoke(new linkFinderActionIMPL(this.uriLink, this));
        //Iterator<String> links = visitedLinks.iterator();
        //while (links.hasNext()) {
        //System.out.println(links.next());
        //}
    }

    @Override
    public int size() {
        return visitedLinks.size();
    }

    @Override
    public boolean visited(String link) {
        return visitedLinks.contains(link);
    }

    @Override
    public void addVisited(String link) {
        visitedLinks.add(link);
    }

    @Override
    public void addToGlenlivetCallList(String link) {
        makeCallsToGlenlivet.add(link);
    }

    @Override
    public Collection<String> aCallListToGlenlivet() {
        return makeCallsToGlenlivet;
    }

    public static void main(String[] args) {
        System.out.println("running");
        scrapeIMPL si = new scrapeIMPL("http://www.lillypulitzer.com/section/new-arrivals/1.uts?currentIndex=0&pageSize=61&defaultPageSize=16&firstPageSize=16&mode=viewall&categoryId=1&type=section", 8);
        si.startCrawling();
    }
}
