package scraper;

import org.htmlparser.Parser;
import org.htmlparser.filters.CssSelectorNodeFilter;
import org.htmlparser.filters.NodeClassFilter;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.util.NodeList;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveAction;

/**
 * Created by whill on 12/21/15.
 * ...
 */

public class linkFinderActionIMPL extends RecursiveAction {
    private String url;
    private scrapeIMPL scraper;
    private static final long t0 = System.nanoTime();

    public linkFinderActionIMPL(String url, scrapeIMPL scraper) {
        this.url = url;
        this.scraper = scraper;
    }

    public void scrapeTopLevel() {
        if (!scraper.visited(url)) {
            try {
                URL urlLink = new URL(url);
                Parser parser = new Parser(urlLink.openConnection());
                NodeList list2 = parser.extractAllNodesThatMatch(new CssSelectorNodeFilter("div.imageDisplay a"));

                for (int i = 0; i < list2.size(); i++) {
                    LinkTag lt = (LinkTag) list2.elementAt(i);

                    if (!lt.extractLink().isEmpty() && !scraper.visited(lt.extractLink())) {
                        //System.out.println(
                        //"<a href='" + lt.getLink() + "'>" + lt.getLinkText().trim() + "</a>"
                        //);
                        scraper.addToGlenlivetCallList(lt.getLink());
                        //URL callMeMaybe = new URL(
                        //"" +
                        //"" + lt.getLink());
                        //callMeMaybe.openConnection();
                    }
                }
                //Should have the section/category link only i.e. 1 in this path.
                scraper.addVisited(url);
            } catch(Exception e) {
                System.err.println(e);
            }
        }
    }

    @Override
    protected void compute() {
        if (!scraper.visited(url)) {
            try {
                List<RecursiveAction> recursiveActions = new ArrayList<RecursiveAction>();
                URL urlLink = new URL(url);
                Parser parser = new Parser(urlLink.openConnection());
                NodeList list = parser.extractAllNodesThatMatch(new NodeClassFilter(LinkTag.class));

                System.out.println(list.size());
                for (int i = 0; i < list.size(); i++) {
                    LinkTag extracted = (LinkTag) list.elementAt(i);

                    if (!extracted.extractLink().isEmpty() && !scraper.visited(extracted.extractLink())) {
                        recursiveActions.add(new linkFinderActionIMPL(extracted.extractLink(), scraper));
                    }
                }
                scraper.addVisited(url);

                invokeAll(recursiveActions);
            } catch(Exception e) {
                System.err.println(e);
            }
        }
    }
}
